FROM bitnami/git:latest as BUILD
COPY app-version /tmp/
RUN cd /tmp && \
    git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/ladigitale/digibuzzer/digibuzzer-sources.git && \
    cd digibuzzer-sources && \
    git checkout $(cat ../app-version)
FROM hub.eole.education/proxyhub/library/node:16 as BUILDER
RUN npm install -g npm@8.19.4
RUN npm install -g vite
RUN mkdir /src
COPY --from=BUILD /tmp/digibuzzer-sources /src
ADD . /src
WORKDIR src
RUN npm install
RUN npm run build
ENTRYPOINT ["npm", "run", "prod"]
EXPOSE 3000

